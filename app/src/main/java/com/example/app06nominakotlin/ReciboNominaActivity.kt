package com.example.app06nominakotlin

import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class ReciboNominaActivity : AppCompatActivity() {

    private lateinit var btnRegresar:Button
    private lateinit var txtNombre: EditText
    private lateinit var txtNumeroRe: EditText
    private lateinit var txtHorasN: EditText
    private lateinit var txtHorasE: EditText
    private lateinit var lblNombre: TextView
    private lateinit var btnLimpiar: Button
    private lateinit var btnCalcular: Button
    private lateinit var recibo: ReciboNomina
    private lateinit var rbtnAuxiliar: RadioButton
    private lateinit var rbtnAlbañil: RadioButton
    private lateinit var rbtnIng: RadioButton
    private lateinit var lblSubtotal: TextView
    private lateinit var lblImpuesto: TextView
    private lateinit var lblTotal: TextView

    private fun iniciar()
    {
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNombre = findViewById(R.id.txtNombre)
        lblNombre = findViewById(R.id.lblNombre)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCalcular = findViewById(R.id.btnCalcular)
        txtHorasE = findViewById(R.id.txtHorasExtras)
        txtHorasN = findViewById(R.id.txtHorasNormales)
        txtNumeroRe = findViewById(R.id.txtNumRecibo)
        rbtnAuxiliar = findViewById(R.id.rbtnAuxiliar)
        rbtnAlbañil = findViewById(R.id.rbtnAlbañil)
        rbtnIng = findViewById(R.id.rbtnIng)
        lblImpuesto = findViewById(R.id.lblImpuesto)
        lblSubtotal = findViewById(R.id.lblSubtotal)
        lblTotal = findViewById(R.id.lblTotal)
    }
    private fun btnLimpiar()
    {
        txtNumeroRe.setText("")
        txtNombre.setText("")
        txtHorasN.setText("")
        txtHorasE.setText("")
        rbtnAlbañil.isChecked = false
        rbtnIng.isChecked = false
        rbtnAuxiliar.isChecked = false
        lblSubtotal.text = "Subtotal: "
        lblImpuesto.text = "Impuesto: "
        lblTotal.text = "Total: "
        recibo = ReciboNomina(0,"",0.0f,0.0f,0,0.0f)
    }
    private fun btnCalcular()
    {
        var puesto = 1
        if (rbtnAlbañil.isChecked) {
            puesto = 2
        } else if (rbtnIng.isChecked) {
            puesto = 3
        }
        recibo = ReciboNomina(
            txtNumeroRe.text.toString().toInt(),
            txtNombre.text.toString(),
            txtHorasN.text.toString().toInt().toFloat(),
            txtHorasE.text.toString().toInt().toFloat(),
            puesto,
            0.0f
        )
        var subtotal = recibo.calcularSubtotal()
        var impuesto = recibo.calcularImpuesto()
        var total = recibo.calcularTotal()
        lblTotal.text = "Total:$total$"
        lblImpuesto.text = "Impuesto:$impuesto$"
        lblSubtotal.text = "Subtotal:$subtotal$"
    }
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)
        iniciar()
        var recibeD = intent.extras
        var nombre = recibeD!!.getString("Nombre")
        lblNombre.text = "Empleado: $nombre"
        txtNombre.setText(nombre)

        btnRegresar.setOnClickListener { finish() }
        btnLimpiar.setOnClickListener { btnLimpiar() }
        btnCalcular.setOnClickListener {
            if (validar()) {
                btnCalcular()
            } else {
                Toast.makeText(applicationContext, "Faltó capturar datos ", Toast.LENGTH_SHORT)
                    .show()
                txtNumeroRe.requestFocus()
            }
        }
    }
    private fun validar(): Boolean
    {
        var exito = true
        Log.d("Nombre", "validar: " + txtNombre.text)
        if (txtNumeroRe.text.toString() == "") exito = false
        if (txtNombre.text.toString() == "") exito = false
        if (txtHorasE.text.toString() == "") exito = false
        if (txtHorasN.text.toString() == "") exito = false
        exito =
            if (rbtnIng.isChecked || rbtnAlbañil.isChecked || rbtnAuxiliar.isChecked) true else false
        return exito
    }
}