package com.example.app06nominakotlin

class ReciboNomina
{
    var numRecibo: Int = 0;
    var nombre: String = "";
    var horasTrabNormales: Float = 0.0f;
    var horasTrabExtras: Float = 0.0f;
    var puesto: Int = 0;
    var ImpuestoPorc: Float = 0.0f;

    constructor(numRecibo: Int, nombre: String, horasTrabNormales: Float, horasTrabExtras: Float, puesto: Int, impuestoPorc: Float)
    {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormales = horasTrabNormales;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.ImpuestoPorc = impuestoPorc;
    }

    fun calcularSubtotal(): Float {
        var subtotal = 0.0f
        var pNormal = 0.0f
        var pExtra = 0.0f
        if (puesto == 1)
        {
            pNormal = 50.0f
            pExtra = 100.0f
        } else if (puesto == 2) {
            pNormal = 70.0f
            pExtra = 140.0f
        } else {
            pNormal = 100.0f
            pExtra = 200.0f
        }
        subtotal = pNormal * horasTrabNormales + pExtra * horasTrabExtras
        return subtotal
    }

    fun calcularImpuesto(): Float {
        var impuesto = calcularSubtotal()
        impuesto = impuesto * 0.16f
        return impuesto
    }

    fun calcularTotal(): Float {
        return calcularSubtotal() - calcularImpuesto()
    }
}