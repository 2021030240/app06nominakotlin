package com.example.app06nominakotlin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var btnEntrar: Button
    private lateinit var txtNombre: EditText
    private lateinit var btnSalir: Button
    private fun iniciar()
    {
        txtNombre = findViewById(R.id.txtNombre)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
    }
    private fun aceptar() {finish()}
    private fun setBtnCerrar()
    {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("¿CERRAR APP?")
        confirmar.setMessage("Se descartara Toda Informacion Ingresada")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialogInterface, which -> aceptar() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialogInterface, which -> }
        confirmar.show()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciar()
        btnEntrar.setOnClickListener {
            if(validar())
            {
                val bundle = Bundle()
                bundle.putString("Nombre", txtNombre.text.toString())
                val intent = Intent(this@MainActivity,ReciboNominaActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }else
            {
                Toast.makeText(applicationContext, "Faltó capturar datos ", Toast.LENGTH_SHORT).show()
                txtNombre.requestFocus()
            }
        }
        btnSalir.setOnClickListener { setBtnCerrar() }
    }
    private fun validar(): Boolean {
        var exito = true
        Log.d("Nombre", "validar: " + txtNombre.text)
        if (txtNombre.text.toString() == "") exito = false
        return exito
    }
}